import * as d3 from 'd3'

export const state = () => ({
  config: {},
  data: []
})

export const mutations = {
  set (state, payload) {
    state[payload.key] = payload.value
  }
}

export const actions = {
  async fetchConfig ({ commit }) {
    // Obtengo el json de configuracion
    const response = await fetch('config.json')
    const config = await response.json()

    commit('set', {
      key: 'config',
      value: config
    })
  },

  async fetchData ({ dispatch, commit, state }) {
    await dispatch('fetchConfig')

    // Obtengo el csv de datos
    const data = await d3.csv(state.config.data_url)

    commit('set', {
      key: 'data',
      value: data
    })
  }
}
